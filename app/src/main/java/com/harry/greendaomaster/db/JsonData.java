package com.harry.greendaomaster.db;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * @author Martin-harry
 * @date 2022/1/5
 * @address
 * @Desc 模拟数据缓存
 */
@Entity
public class JsonData {
    @Id(autoincrement = true)
    private Long id;
    private String json;//json实体
    @Generated(hash = 360526212)
    public JsonData(Long id, String json) {
        this.id = id;
        this.json = json;
    }
    @Generated(hash = 1032513285)
    public JsonData() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getJson() {
        return this.json;
    }
    public void setJson(String json) {
        this.json = json;
    }
}
