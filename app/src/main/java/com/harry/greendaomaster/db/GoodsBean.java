package com.harry.greendaomaster.db;

import java.util.List;

/**
 * @author Martin-harry
 * @date 2022/1/5
 * @address
 * @Desc 模拟数据实体类
 */
public class GoodsBean {
    private String msg;
    private String code;
    private String page;
    private List<DataBean> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private String foods;
        private String money;

        public String getFoods() {
            return foods;
        }

        public void setFoods(String foods) {
            this.foods = foods;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }
    }
}
