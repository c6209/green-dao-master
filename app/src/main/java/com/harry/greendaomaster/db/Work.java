package com.harry.greendaomaster.db;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Work {
    @Id(autoincrement = true)
    private Long id;
    private int number; // 设置唯一索引
    private String userName;
    private String worker;
    @Generated(hash = 1484553769)
    public Work(Long id, int number, String userName, String worker) {
        this.id = id;
        this.number = number;
        this.userName = userName;
        this.worker = worker;
    }
    @Generated(hash = 572069219)
    public Work() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public int getNumber() {
        return this.number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public String getUserName() {
        return this.userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getWorker() {
        return this.worker;
    }
    public void setWorker(String worker) {
        this.worker = worker;
    }
}
