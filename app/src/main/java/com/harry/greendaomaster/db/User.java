package com.harry.greendaomaster.db;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Unique;

/**
 * @author 拉莫帅
 * @date 2022/1/5
 * @address
 * @Desc 人物
 *
 * 创建数据库实体类
 *
 *      @Entity 表示这个实体类一会会在数据库中生成对应的表
 *
 *      @Id 表示该字段是id，注意该字段的数据类型为包装类型Long
 *
 *      @Property 则表示该属性将作为表的一个字段，其中nameInDb看名字就知道这个属性在数据库中对应的数据名称
 *
 *       @Transient 该注解表示这个属性将不会作为数据表中的一个字段
 *
 *       @NotNull 表示该字段不可以为空
 *
 *       @Unique 表示该字段唯一
 */
@Entity
public class User {
    @Id(autoincrement = true)
    private Long id;
    @Unique
    private int number; // 设置唯一索引
    private String username;
    private String password;
    @Generated(hash = 587458021)
    public User(Long id, int number, String username, String password) {
        this.id = id;
        this.number = number;
        this.username = username;
        this.password = password;
    }
    @Generated(hash = 586692638)
    public User() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public int getNumber() {
        return this.number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public String getUsername() {
        return this.username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return this.password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
