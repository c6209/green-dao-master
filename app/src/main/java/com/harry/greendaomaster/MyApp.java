package com.harry.greendaomaster;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.harry.greendaomaster.greendao.gen.DaoMaster;
import com.harry.greendaomaster.greendao.gen.DaoSession;

/**
 * @author 拉莫帅
 * @date 2022/1/5
 * @address
 * @Desc 全局初始化
 */
public class MyApp extends Application {

    private DaoSession mDaoSession;
    public static MyApp instances;
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        instances = this;
        context = getApplicationContext();
        setDatabase();
    }

    public static MyApp getInstances() {
        return instances;
    }

    public static Context getAppContext() {
        return context;
    }

    /**
     * 设置greenDao
     */
    private void setDatabase() {
        DaoMaster.DevOpenHelper mHelper = new DaoMaster.DevOpenHelper(this, "harry-db", null);
        SQLiteDatabase db = mHelper.getWritableDatabase();
        DaoMaster mDaoMaster = new DaoMaster(db);
        mDaoSession = mDaoMaster.newSession();
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }
}
