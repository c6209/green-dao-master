package com.harry.greendaomaster.base;

import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;

/**
 * @author 拉莫帅
 * @date 2021/7/13
 * @address
 * @Desc activity基类
 */
public abstract class BaseActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(createViews());//初始化视图
        initView();//初始化控件
        initData();//初始化数据
    }

    protected abstract void initView();

    protected abstract void initData();

    protected abstract int createViews();

}
