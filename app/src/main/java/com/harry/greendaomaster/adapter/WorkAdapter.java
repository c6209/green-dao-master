package com.harry.greendaomaster.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.harry.greendaomaster.R;
import com.harry.greendaomaster.db.User;
import com.harry.greendaomaster.db.Work;

import java.util.List;

public class WorkAdapter extends RecyclerView.Adapter<WorkAdapter.MyViewHolder> {
    private Context mContext;
    private List<Work> list;

    public WorkAdapter(Context context, List<Work> list) {
        this.mContext = context;
        this.list = list;
    }

    @NonNull
    @Override
    public WorkAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        WorkAdapter.MyViewHolder holder = new WorkAdapter.MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.adapter_item, parent, false));
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull WorkAdapter.MyViewHolder holder, int position) {
        if (holder != null) {
            holder.mId.setText(list.get(position).getId() + "");
            holder.mNumber.setText(list.get(position).getNumber() + "");
            holder.mName.setText(list.get(position).getUserName() + "");
            holder.mWork.setText(list.get(position).getWorker() + "");
            holder.mPwd.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mId;
        private TextView mNumber;
        private TextView mName;
        private TextView mWork;
        private TextView mPwd;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            mId = itemView.findViewById(R.id.id);
            mNumber = itemView.findViewById(R.id.number);
            mName = itemView.findViewById(R.id.name);
            mWork = itemView.findViewById(R.id.work);
            mPwd = itemView.findViewById(R.id.pwd);
        }
    }
}
