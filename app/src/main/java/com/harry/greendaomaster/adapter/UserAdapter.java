package com.harry.greendaomaster.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.harry.greendaomaster.R;
import com.harry.greendaomaster.db.User;

import java.util.List;

/**
 * @author 拉莫帅
 * @date 2022/1/5
 * @address
 * @Desc 人物adapter
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder> {
    private Context mContext;
    private List<User> list;

    public UserAdapter(Context context, List<User> list) {
        this.mContext = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MyViewHolder holder = new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.adapter_item, parent, false));
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (holder != null) {
            holder.mId.setText(list.get(position).getId() + "");
            holder.mNumber.setText(list.get(position).getNumber() + "");
            holder.mName.setText(list.get(position).getUsername() + "");
            holder.mPwd.setText(list.get(position).getPassword() + "");
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mId;
        private TextView mNumber;
        private TextView mName;
        private TextView mPwd;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            mId = itemView.findViewById(R.id.id);
            mNumber = itemView.findViewById(R.id.number);
            mName = itemView.findViewById(R.id.name);
            mPwd = itemView.findViewById(R.id.pwd);
        }
    }
}
