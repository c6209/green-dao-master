package com.harry.greendaomaster.util;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;

/**
 * @author Martin-harry
 * @date 2021/12/10
 * @address
 * @Desc 检查网络设置
 */
public class NetUtilsWork {
    /**
     * 判断手机是否联网
     *
     * @param context
     * @return
     */
    public static boolean isConn(Context context) {
        boolean ConnFlag = false;
        ConnectivityManager conManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo network = conManager.getActiveNetworkInfo();
        if (network != null) {
            ConnFlag = conManager.getActiveNetworkInfo().isAvailable();
        }
        return ConnFlag;
    }

    /**
     * 打开网络设置
     *
     * @param context
     */
    public static void openNetSettingDg(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("设置网络");
        builder.setMessage("是否要打开网络连接？");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = null;
                // 判断当前系统版本
                if (Build.VERSION.SDK_INT >= 10) {
                    // 3.0以上
                    intent = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
                } else {
                    intent = new Intent();
                    ComponentName component = new ComponentName("com.android.settings", "com.android.settings.WirelessSettings");
                    intent.setComponent(component);
                    intent.setAction("android.intent.action.VIEW");
                }
                context.startActivity(intent);
            }
        });
        builder.setNegativeButton("取消", null);

        builder.show();
    }
}
